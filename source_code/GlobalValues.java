/**
 * Created by Vlad-minihp on 09/11/2016.
 */
public class GlobalValues {
    private static GlobalValues instance = null;
    public int gameMode; // 0 for 1VS1  / 1 for 1VSPC
    public int playerTurn; //0 for player 1, 1 for player 2 or PC
    public Player[] playerList = new Player[2]; //Small vector of Players that helps us deal with calling their respective methods easily by calling for example playerList[1].method().
    public int[][] board = new int[2][6];
    private GlobalValues(){} //Private constructor so we will never create new objects of this class by mistake.
    public static GlobalValues getInstance() { // This method will check if there is an existing instance of this object, if not it will create one and return it for further use in the other clases.
        if (instance == null)
            instance = new GlobalValues();
        return instance;
    }
}
