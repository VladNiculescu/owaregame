import java.util.Random;

/**
 * Created by Vlad-minihp on 09/11/2016.
 */
public class GameLogic extends Thread{
    private static GameLogic instance = null;
    private GameScreen gameScreen;
    private GlobalValues gvInstance = GlobalValues.getInstance();
    private GameLogic(){}
    public static GameLogic getInstance(){
        if(instance==null)
            instance = new GameLogic();
        return instance;
    }

    public void startGame(){
        initializeBoard();
        generatePlayers();
        randomPlayer();
    }
    public void showBoard(GameScreen gameScreen){
        this.gameScreen = gameScreen;
        gameScreen.launchBoard();
    }
    public void initializeBoard(){
        // Initializing the board
        for(int i=0;i<6;++i){
            for(int j=0;j<2;++j)
                gvInstance.board[j][i]=4;
        }

    }
    public void generatePlayers(){
        if(gvInstance.gameMode==0){
            gvInstance.playerList[0] = new Player("Player 1",0);
            gvInstance.playerList[1] = new Player("Player 2",1);
        }else{
            gvInstance.playerList[0] = new PC();
            gvInstance.playerList[1] = new Player("Player 1",1);
        }
    }
    public void randomPlayer(){
        if(gvInstance.gameMode==0){
            Random rand = new Random();
            gvInstance.playerTurn =  rand.nextInt(1);
        }else gvInstance.playerTurn = 1;
    }

    public void play(int playerHouse,int player){
        if(player==gvInstance.playerTurn){
            if(gvInstance.gameMode==1) {
                if(gvInstance.playerList[gvInstance.playerTurn] instanceof  PC)
                    ((PC) gvInstance.playerList[gvInstance.playerTurn]).playRandomTurn();
                else gvInstance.playerList[gvInstance.playerTurn].playTurn(playerHouse);
            }
            else if(gvInstance.gameMode==0)
                gvInstance.playerList[gvInstance.playerTurn].playTurn(playerHouse);
            else if(gvInstance.gameMode==2) {
                if(gvInstance.playerList[gvInstance.playerTurn] instanceof  PC)
                    ((PC) gvInstance.playerList[gvInstance.playerTurn]).playAITurn();
                else gvInstance.playerList[gvInstance.playerTurn].playTurn(playerHouse);
            }
        }

        if(gvInstance.gameMode==1 && gvInstance.playerTurn==0){
            PC computer = (PC) gvInstance.playerList[0];
            computer.playRandomTurn();
        }
        if(gvInstance.gameMode==2 && gvInstance.playerTurn==0){
            PC computer = (PC) gvInstance.playerList[0];
            computer.playAITurn();
        }
    }
    public int checkWinner(){
        if(gvInstance.playerList[0].getScore()>24)  return 0;
        else if(gvInstance.playerList[1].getScore()>24) return 1;
        return -1;
    }
}