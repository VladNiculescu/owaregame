import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.*;

import javafx.stage.Stage;
import javafx.scene.text.*;

import java.util.Optional;


public class GameScreen extends Application {
    public Button buttons[][] = new Button[2][6];
    Text playerOneScore, playerTwoScore;
    Stage stage;

   /* public static void main(String[] args) {
        launch(args);
    }*/
    public void launchBoard(){
       launch();
    }
    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Oware Game");
        StackPane root = new StackPane();

        /* DECLARATIONS */
        BorderPane mainPane = new BorderPane();
        BorderPane topPane = new BorderPane();
        GridPane grid = new GridPane();
        playerOneScore = new Text();
        playerTwoScore = new Text();

        BorderPane playerOneScoreHolder = new BorderPane();
        BorderPane playerTwoScoreHolder = new BorderPane();

        Text player1 = new Text(" "+GlobalValues.getInstance().playerList[1].getName());
        Text player2 = new Text(" "+GlobalValues.getInstance().playerList[0].getName());

        player1.setFont(new Font(30));
        player2.setFont(new Font(30));


        playerOneScoreHolder.setBottom(player1);
        playerOneScoreHolder.setCenter(playerOneScore);

        playerTwoScoreHolder.setTop(player2);
        playerTwoScoreHolder.setCenter(playerTwoScore);


        TilePane topTile = new TilePane();
        TilePane bottomTile = new TilePane();
        Button counter = new Button("End Game");

        counter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                if(GlobalValues.getInstance().playerList[0].getScore()>GlobalValues.getInstance().playerList[1].getScore())
                {
                   showDialog(alert,"Game Over",GlobalValues.getInstance().playerList[0].getName()+" wins!",stage);

                }
                else if (GlobalValues.getInstance().playerList[0].getScore()<GlobalValues.getInstance().playerList[1].getScore()) showDialog(alert,"Game Over",GlobalValues.getInstance().playerList[1].getName()+" wins!",stage);
                else showDialog(alert,"Game Over","It's a draw!",stage);

            }
        });

        /* ASSESMENTS */
        topTile.setAlignment(Pos.CENTER);
        bottomTile.setAlignment(Pos.CENTER);


        playerOneScore.setText("0");
        playerTwoScore.setText("0");

        playerOneScore.setId("title");
        playerTwoScore.setId("title");

        counter.getStyleClass().add("button-counter");

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);



        for(int i=0;i<6;i++){

            StackPane pane = new StackPane();

            pane.getStyleClass().add("game-grid-cell-top");
            pane.getStyleClass().add("player-2");


            /* Adding unique ID to eleemnt for animating */
            String id="button-top-";
            Integer index = i;
            id+= index.toString();


            Button circle = new Button();
            circle.setText(""+GlobalValues.getInstance().board[0][i]);
            circle.getStyleClass().add("button-circle");
            //ANMATE

            circle.setFont(new Font(30));
            circle.setId(id);
            buttons[0][i] = circle;


            int row;
            row=0;
            pane.getChildren().add(circle);
            grid.add(pane,i,row);
        }


        for(int i=0;i<6;i++){
            Pane pane = new Pane();
            pane.getStyleClass().add("game-grid-cell-bottom");
            pane.getStyleClass().add("player-1");
            pane.getStyleClass().add("active");



            /* Adding unique ID to eleemnt for animating */
            String id="button-bottom-";
            Integer index = i;
            id+= index.toString();


            Button circle = new Button();
            circle.setText(""+GlobalValues.getInstance().board[1][i]);
            circle.getStyleClass().add("button-circle");
            circle.setId(id);
           circle.setFont(new Font(30));
            buttons[1][i] = circle;

            int row;
            row=1;
            pane.getChildren().add(circle);
            grid.add(pane,i,row);
        }

        for(int i=0;i<2;i++){

            for(int j=0;j<6;j++){
                final int a = i;final int b=j;
                buttons[i][j].setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {

                        GameLogic.getInstance().play(b,a);

                        if(GlobalValues.getInstance().playerTurn!=a)  updateTurn();
                        update(primaryStage);

                    }
                });
            }
        }


        /* PLUG THINGS TOGETHER */

        topTile.getChildren().add(playerTwoScoreHolder);
        bottomTile.getChildren().add(playerOneScoreHolder);

        topPane.setBottom(topTile);
        topPane.setRight(counter);
        mainPane.setTop(topPane);
        mainPane.setBottom(bottomTile);
        mainPane.setCenter(grid);
        root.getChildren().add(mainPane);
         updateTurn();
        /* PUSH PLAY */

        Scene scene = new Scene(root, 800, 600);
        primaryStage.setMinHeight(600);
        primaryStage.setMinWidth(800);
        scene.getStylesheets().add("/style.css");
        primaryStage.setScene(scene);
        stage = primaryStage;
       primaryStage.show();
    }
    private void showDialog(Alert alert,String title,String context,Stage primaryStage){
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(context);

        Optional<ButtonType> result=  alert.showAndWait();
        if (result.get() == ButtonType.OK) primaryStage.close();
    }
    public void update(Stage stage) {
        updateScores();
        updateBoard();
        if(GameLogic.getInstance().checkWinner()!=-1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            showDialog(alert,"Game Over",""+ GlobalValues.getInstance().playerList[GameLogic.getInstance().checkWinner()].getName()+ " wins",stage);

        }

    }
    private void updateScores()  {
        playerTwoScore.setText(""+GlobalValues.getInstance().playerList[0].getScore());
        playerOneScore.setText(""+GlobalValues.getInstance().playerList[1].getScore());
    }
    private void updateBoard(){
        for(int i=0;i<2;++i)
            for(int j =0;j<6;++j) {
                buttons[i][j].setText(""+GlobalValues.getInstance().board[i][j]);
            }
    }
    private void updateTurn(){
        for(int i=0;i<6;++i)
            if (GlobalValues.getInstance().playerTurn == 1) {
             buttons[1][i].getStyleClass().remove("active-button");
                buttons[1][i].getStyleClass().add("active-button");
                buttons[0][i].getStyleClass().remove("active-button");

            } else if (GlobalValues.getInstance().playerTurn == 0) {
                buttons[0][i].getStyleClass().remove("active-button");
                buttons[0][i].getStyleClass().add("active-button");
                buttons[1][i].getStyleClass().remove("active-button");
            }
        }

}


