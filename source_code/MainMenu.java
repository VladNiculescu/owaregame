import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import static javafx.application.Application.launch;

/**
 * Created by Vlad-minihp on 09/11/2016.
 */
public class MainMenu extends Application {
  /*Includes:
    1VS1 Button
    1VSPC Button
   */

    public void launchMenu(){
        launch();
    }
  @Override
    public void start(Stage primaryStage) {


        Button sModeBtn = new Button("Player VS PC (Random)");
        Button aiModeBtn = new Button("Player VS PC (AI)");
        Button dModeBtn = new Button("Player VS Player");
        sModeBtn.setMaxWidth(Double.MAX_VALUE);
        dModeBtn.setMaxWidth(Double.MAX_VALUE);
        aiModeBtn.setMaxWidth(Double.MAX_VALUE);
        Text title = new Text("OWARE");
        title.setTextAlignment(TextAlignment.CENTER);
        title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        Text mode = new Text("Choose a player mode");



      /*StackPane root = new StackPane();
      root.getChildren().add(sModeBtn);
      root.getChildren().add(dModeBtn);*/
    //  GameLogic.getInstance().showBoard(new GameScreen());

      aiModeBtn.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
              GlobalValues.getInstance().gameMode = 2;
              primaryStage.close();
              GameLogic.getInstance().startGame();
              new GameScreen().start(new Stage());
          }
      });

     sModeBtn.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             GlobalValues.getInstance().gameMode = 1;
             primaryStage.close();
             GameLogic.getInstance().startGame();
            new GameScreen().start(new Stage());
         }
     });
      dModeBtn.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
              GlobalValues.getInstance().gameMode = 0;
              primaryStage.close();
              GameLogic.getInstance().startGame();
              new GameScreen().start(new Stage());
          }
      });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(title, 0, 0, 2, 1);
        grid.setHalignment(title, HPos.CENTER);
        grid.setValignment(title, VPos.CENTER);
        grid.setHalignment(mode, HPos.CENTER);
        grid.setValignment(mode, VPos.CENTER);
        grid.add(mode, 0, 1, 2,1);
        grid.add(sModeBtn, 1, 4);
        grid.add(dModeBtn, 1, 6);
        grid.add(aiModeBtn,1,  5);

        Scene scene = new Scene(grid, 300, 250);
      primaryStage.setMinHeight(250);
      primaryStage.setMinWidth(300);
        primaryStage.setTitle("Oware Game");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


}
