import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Vlad-minihp on 09/11/2016.
 */
public class PC extends Player {

    public PC(){
        super("PC",0);

    }

    public void playRandomTurn(){
        ArrayList<Integer> validMoves = new ArrayList<Integer>();
        Random random = new Random();
        for(int i=0; i<6; ++i) {
            if(canMove(i)){
                validMoves.add(i);

            }

        }
        if(validMoves.size()>0){
            random.nextInt(validMoves.size());
            int n = random.nextInt(validMoves.size());
            playTurn(validMoves.get(n));
        }


    }
    int[] targetHouse;
    public void playAITurn(){
        int max =0;
        int maxindex =-1;
        int secIndex =-1;
        for(int i=0; i<6; ++i) {
            if(canMove(i)){
                getTargetHouse(i);
                if(targetHouse[0]==1) {
                    if (GlobalValues.getInstance().board[1][targetHouse[1]] == 2 || GlobalValues.getInstance().board[1][targetHouse[1]] == 3) {
                        int captureNo = chainCapture(i);
                        if (captureNo > max && (checkSeeds(i, i + captureNo))) {
                            max = chainCapture(i);
                            maxindex = i;
                        }
                    }
                    if (GlobalValues.getInstance().board[1][targetHouse[1]] == 0 || GlobalValues.getInstance().board[1][targetHouse[1]] == 1) {
                        secIndex = i;
                    }
                }
            }

        }
        if(maxindex!=-1){
            playTurn(maxindex);

        }else if(secIndex!=-1){
            playTurn(secIndex);

        }else {
            playRandomTurn();

        }
    }

    public void getTargetHouse(int startHouse) {
        int seeds = GlobalValues.getInstance().board[0][startHouse];
        //empties the house

        //the coordinates of the next house
        int[] indexes = nextCoordinates(0, startHouse);
        while (seeds > 0) {
            // it skips the position of startHouse
            if (indexes[0] == 0 && indexes[1] == startHouse)
                indexes = nextCoordinates(indexes[0], indexes[1]);

            --seeds;
            targetHouse = indexes;
            indexes = nextCoordinates(indexes[0], indexes[1]);
        }

    }

}
