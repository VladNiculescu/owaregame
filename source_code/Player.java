/**
 * Created by Vlad-minihp on 09/11/2016.
 */
public class Player {
    private String name;
    private int id;
    private int seedsCaptured = 0;
    private int opponentId;
    private int[] currentTargetHouse;
    private GlobalValues gvInstance = GlobalValues.getInstance();

    public Player(String name, int id) {
        this.name = name;
        this.id = id;
        opponentId = (id + 1) % 2;

    }

    public String getName() {
        return name;
    }

    public void playTurn(int playerHouse) {
        if (canMove(playerHouse)) {
            move(playerHouse);
            if (getTargetHouse()[0] == getOpponentId()) {
                capture(getTargetHouse()[1]);

            }
        }
            //capture(getTargetHouse()[1]);
    }

    public boolean canMove(int startHouse) {
        if (gvInstance.board[id][startHouse] == 0) return false; //Invalid move
        if (!opponentHasSeeds()) //Check if opponent has any seeds
            if (!possibleMove(startHouse)) return false; //check if any movements can supply the opponent with seeds
        return true;
    }

    private boolean opponentHasSeeds() {
        for (int i = 0; i < 6; ++i)
            if (gvInstance.board[opponentId][i] > 0)
                return true;
        return false;
    }

    private boolean possibleMove(int house) {
        boolean possibleMove = false;
        if (gvInstance.board[id][house] > house && id == 0) return true;
        if (gvInstance.board[id][house] >= 6 - house && id == 1) return true;
        return possibleMove;
    }

    public boolean capture(int targetHouse) {
        //1: are there 2 or 3 seeds in the house we landed on?
        if (gvInstance.board[opponentId][targetHouse] == 2 || gvInstance.board[opponentId][targetHouse] == 3) {
            int captures = chainCapture(targetHouse); // number of of chain capture
            if (checkSeeds(targetHouse, targetHouse + captures)) {

                int n;
                if(opponentId == 0){
                    n = targetHouse + captures-1;
                }
                else{
                    n = targetHouse - captures+1;
                }
                for (int i = 0; i < captures; i++) {

                    captureSingle(n);
                    n = nextCoordinates(opponentId, n)[1];
                }
                return true;
            }
        }
        return false;
    }

    public boolean checkSeeds(int start, int end) {
        for (int i = 0; i < 6; i++) {
            if (i < start || i > end) {
                if (gvInstance.board[opponentId][i] > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    // returns ending point of chain capture
    public int chainCapture(int startHouse) {
        int captures = 1;
        if (opponentId == 0) {
            for (int i = startHouse + 1; i < 6; i++) {  // check houses after the beginning capture house
                if (gvInstance.board[opponentId][i] == 2 || gvInstance.board[opponentId][i] == 3) {
                    captures++; // if it has 2 or 3, iterate endCapture
                } else {
                    break; // if you reach a point that has neither 2 or 3, capture ends here
                }
            }
        } else {
            for (int i = startHouse - 1; i > 0; i--) {
                if (gvInstance.board[opponentId][i] == 2 || gvInstance.board[opponentId][i] == 3) {
                    captures++;
                } else {
                    break;
                }
            }
        }

        return captures;
    }

    public void move(int startHouse) {
        //   int totalSeeds = gvInstance.board[id][startHouse]; //Retrieves the number of seeds from that house
        distributeSeeds(startHouse); //Distributes the seeds counter clock-wise
        gvInstance.playerTurn = opponentId; //Change the current turn

    }

    public void distributeSeeds(int startHouse) {
        int remainingSeeds = gvInstance.board[id][startHouse];
        //empties the house
        gvInstance.board[id][startHouse] = 0;
        //the coordinates of the next house
        int[] indexes = nextCoordinates(id, startHouse);
        while (remainingSeeds > 0) {
            // it skips the position of startHouse
            if (indexes[0] == id && indexes[1] == startHouse)
                indexes = nextCoordinates(indexes[0], indexes[1]);
            ++gvInstance.board[indexes[0]][indexes[1]];
            --remainingSeeds;
            currentTargetHouse = indexes;
            indexes = nextCoordinates(indexes[0], indexes[1]);
        }

    }

    public int[] getTargetHouse() {

        return currentTargetHouse;
    }

    // helper method that returns the coordinates of the next house on the board as an array
    protected int[] nextCoordinates(int player, int house) {
        int retPlayer, retHouse;
        if (player == 0) {
            if (house == 0)
                return new int[]{1, 0};
            else
                return new int[]{0, house - 1};
        } else {
            if (house == 5)
                return new int[]{0, 5};
            else
                return new int[]{1, house + 1};
        }

    }

    public void captureSingle(int targetHouse) {
        seedsCaptured += gvInstance.board[opponentId][targetHouse];
        gvInstance.board[opponentId][targetHouse] = 0;
    }

    public int getScore() {
        return seedsCaptured;
    }

    public int getOpponentId() {
        return opponentId;
    }
}
